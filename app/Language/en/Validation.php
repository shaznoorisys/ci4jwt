<?php

// override core en language system validation or define your own en language validation message
return [
    'email' => [
        'required'      =>  'User email address required.',
        'min_length'    =>  'Email address minimum length is {0} characters',
        'max_length'    =>  'Email address maximum length is {0} characters',
        'valid_email'   =>  '{0}, is not a valid email address.',
        'is_unique'     =>  '{0}, email address is already exists.',
    ],
    'password' => [
        'required'      =>  'Password must required.',
        'min_length'    =>  'Password minimum length is {0} characters',
        'max_length'    =>  'Password minimum length is {0} characters',
    ],
    'name' => [
        'required'      =>  'Please enter the user full name.',
        'min_length'    =>  'User full name must have minimum length is {0} characters',
    ],

    'inputs' => [
        'invalid' => 'Invalid input found! please try again...'
    ],
    'users' => [
        'register' => [
            'success'   => 'User successfully registered.',
            'failure'   => 'Unable to register new user. please try again.'
        ],
        'login' => [
            'success'   => 'User successfully logged-In.',
            'failure'   => 'User unable to login, please try with valid credentials',
            'not_found' => 'Unable to find the user, please try with valid credentials'
        ],
        'auth' => [
            'success'   => 'User authenticated.',
            'failure'   => 'User not authorized. please logout and login again...',
        ],
        'password' => [
            'not_matched'   =>  'Old password did not matched! Please try again...',
            'changed'       =>  'Congrats, password successfully changed.',
            'not_changed'   =>  'Unable to change the profile password. Please try again...'
        ],
        'profile' => [
            'success'       =>  'Congrats, profile successfully updated.',
            'failed'        =>  'Congrats, profile successfully updated.',
        ]
    ]
];
