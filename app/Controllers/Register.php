<?php

namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Config\Services;
use Exception;

class Register extends ResourceController
{
    use ResponseTrait;
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        $service = new Services();
        $email      =   $this->request->getVar('email');

        // Set Validation Rules
        $rules = [
            'name' => [
                'rules'         =>  'required|min_length[' . MIN_LENGTH . ']|max_length[' . MAX_LENGTH . ']',
                'errors'        => [
                    'required'      =>  Lang('Validation.name.required'),
                    'min_length'    =>  Lang('Validation.name.min_length', [MIN_LENGTH]),
                    'max_length'    =>  Lang('Validation.email.max_length', [MAX_LENGTH])
                ]
            ],
            'email' => [
                'rules'         =>  'required|min_length[' . EMAIL_MIN_LENGTH . ']|max_length[' . EMAIL_MAX_LENGTH . ']|valid_email|is_unique[users.email]',
                'errors'        => [
                    'required'      =>  Lang('Validation.email.required'),
                    'min_length'    =>  Lang('Validation.email.min_length', [EMAIL_MIN_LENGTH]),
                    'max_length'    =>  Lang('Validation.email.max_length', [EMAIL_MAX_LENGTH]),
                    'valid_email'   =>  Lang('Validation.email.valid_email', [$email]),
                    'is_unique'     =>  Lang('Validation.email.is_unique', [$email])
                ]
            ],
            'password' => [
                'rules'         => 'required|min_length[' . PASSWORD_MIN_LENGTH . ']|max_length[' . PASSWORD_MAX_LENGTH . ']',
                'errors'        => [
                    'required'      =>  Lang('Validation.password.required'),
                    'min_length'    =>  Lang('Validation.password.min_length', [PASSWORD_MIN_LENGTH]),
                    'max_length'    =>  Lang('Validation.password.max_length', [PASSWORD_MAX_LENGTH])
                ]
            ]
        ];

        if (!$this->validate($rules)) {
            return $service->fail(
                [
                    'errors'     =>  $this->validator->getErrors(),
                    'message'   =>  Lang('Validation.inputs.invalid'),
                ],
                ResponseInterface::HTTP_BAD_REQUEST,
                $this->response
            );
        }

        $password   =   $this->request->getVar('password');
        $inputs     =   [
            'name'      =>  $this->request->getVar('name'),
            'email'     =>  $email,
            'password'  =>  $password
        ];

        try {
            $model  =   new UserModel();
            $model->save($inputs);

            $user = $model->where('email', $email)->first();
            unset($user['password']);

            return $service->success(
                [
                    'message'       =>  Lang('Validation.users.register.success'),
                    'data'          =>  [
                        'user'          =>  $user,
                        'access_token'  =>  Services::getSignedAccessTokenForUser($email)
                    ]
                ],
                ResponseInterface::HTTP_CREATED,
                $this->response
            );
        } catch (Exception $e) {
            return $service->fail(
                [
                    'errors'    =>  (object) ['error' => $e->getMessage()],
                    'message'   =>  Lang('Validation.users.register.failure'),
                ],
                ResponseInterface::HTTP_BAD_REQUEST,
                $this->response
            );
        }
    }
}
