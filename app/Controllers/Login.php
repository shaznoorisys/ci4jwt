<?php

namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Config\Services;
use Exception;

class Login extends ResourceController
{
    use ResponseTrait;
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        $service    =   new Services();
        $email      =   $this->request->getVar('email');

        // Set Validation Rules
        $rules = [
            'email' => [
                'rules'         =>  'required|min_length[' . EMAIL_MIN_LENGTH . ']|max_length[' . EMAIL_MAX_LENGTH . ']|valid_email',
                'errors'        => [
                    'required'      =>  Lang('Validation.email.required'),
                    'min_length'    =>  Lang('Validation.email.min_length', [EMAIL_MIN_LENGTH]),
                    'max_length'    =>  Lang('Validation.email.max_length', [EMAIL_MAX_LENGTH]),
                    'valid_email'   =>  Lang('Validation.email.valid_email', [$email]),
                ]
            ],
            'password' => [
                'rules'         => 'required|min_length[' . PASSWORD_MIN_LENGTH . ']|max_length[' . PASSWORD_MAX_LENGTH . ']',
                'errors'        => [
                    'required'      =>  Lang('Validation.password.required'),
                    'min_length'    =>  Lang('Validation.password.min_length', [PASSWORD_MIN_LENGTH]),
                    'max_length'    =>  Lang('Validation.password.max_length', [PASSWORD_MAX_LENGTH])
                ]
            ]
        ];

        if (!$this->validate($rules)) {
            return $service->fail(
                [
                    'errors'     =>  $this->validator->getErrors(),
                    'message'   =>  Lang('Validation.inputs.invalid'),
                ],
                ResponseInterface::HTTP_BAD_REQUEST,
                $this->response
            );
        }

        $password   =   $this->request->getVar('password');

        $model  =   new UserModel();
        try {

            if (!is_null($user   =   $model->where('email', $email)->first())) {
                if (password_verify($password, $user['password'])) {
                    unset($user['password']);
                    return $service->success(
                        [
                            'message'       =>  Lang('Validation.users.login.success'),
                            'data'          =>  [
                                'user'          =>  $user,
                                'access_token'  =>  Services::getSignedAccessTokenForUser($email)
                            ]
                        ],
                        ResponseInterface::HTTP_CREATED,
                        $this->response
                    );
                }
            }
            return $service->fail(
                [
                    'errors'    =>  "",
                    'message'   =>  Lang('Validation.users.login.failure'),
                ],
                ResponseInterface::HTTP_BAD_REQUEST,
                $this->response
            );
        } catch (Exception  $e) {
            return $service->fail(
                [
                    'errors'    =>  "",
                    'message'   =>  Lang('Validation.users.login.failure'),
                ],
                ResponseInterface::HTTP_BAD_REQUEST,
                $this->response
            );
        }
    }
}
