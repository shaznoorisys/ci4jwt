<?php

namespace App\Controllers\Users;

use App\Models\UserModel;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Config\Services;

class Profile extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        $service = new Services();
        $model  = new UserModel();
        if (($email = $this->request->email) && ($user = $model->where('email', $email)->first())) {
            unset($user['password']);
            return $service->success(
                [
                    'message'       =>  Lang('Validation.users.auth.success'),
                    'data'          =>  [
                        'user'          =>  $user
                    ]
                ],
                ResponseInterface::HTTP_CREATED,
                $this->response
            );
        } else {
            return $service->fail(
                [
                    'errors'    =>  "",
                    'message'   =>  Lang('Validation.users.auth.failure'),
                ],
                ResponseInterface::HTTP_BAD_REQUEST,
                $this->response
            );
        }
    }

    public function updateProfile()
    {
        $service    =   new Services();
        $email      =   $this->request->email;
        $model      =   new UserModel();
        // Validate User by email
        if ($user = $model->where('email', $email)->first()) {
            $inputs = [
                "id"        =>  $user['id'],
                "name"      =>  $this->request->getVar('name'),
                "address"   =>  $this->request->getVar('address'),
                "city"      =>  $this->request->getVar('city'),
                "state"     =>  $this->request->getVar('state'),
                "country"   =>  $this->request->getVar('country')
            ];

            if ($model->save($inputs)) {
                return $service->success(
                    [
                        'message'       =>  Lang('Validation.users.profile.success'),
                        'data'          =>  ""
                    ],
                    ResponseInterface::HTTP_OK,
                    $this->response
                );
            }
        }

        return $service->fail(
            [
                'errors'    =>  "",
                'message'   =>  Lang('Validation.users.profile.failed'),
            ],
            ResponseInterface::HTTP_BAD_REQUEST,
            $this->response
        );
    }
}
