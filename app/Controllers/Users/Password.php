<?php

namespace App\Controllers\Users;

use App\Models\UserModel;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Config\Services;
use Exception;

class Password extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        $service        =   new Services();
        $model          =   new UserModel();

        $old_password   =   $this->request->getVar('old_password');
        $new_password   =   $this->request->getVar('new_password');

        try {
            $email      =   $this->request->email;
            $user       =   $model->where('email', $email)->first();
            if (password_verify($old_password, $user['password'])) {
                $inputs = [
                    "id"     =>  $user['id'],
                    "password"  =>  $new_password
                ];
                $model->save($inputs);

                return $service->success(
                    [
                        'message'       =>  Lang('Validation.users.password.changed'),
                        'data'          =>  ""
                    ],
                    ResponseInterface::HTTP_CREATED,
                    $this->response
                );
            } else {
                return $service->fail(
                    [
                        'errors'    =>  "",
                        'message'   =>  Lang('Validation.users.password.not_matched'),
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST,
                    $this->response
                );
            }
        } catch (Exception $e) {
            return $service->fail(
                [
                    'errors'    =>  "",
                    'message'   =>  Lang('Validation.users.password.not_changed'),
                ],
                ResponseInterface::HTTP_BAD_REQUEST,
                $this->response
            );
        }
    }

    public function forgotPassword()
    {
    }
}
