<?php

namespace Config;

use CodeIgniter\Config\BaseService;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\I18n\Time;
use \Firebase\JWT\JWT;
use \Firebase\JWT\Key;



/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends BaseService
{
    /*
     * public static function example($getShared = true)
     * {
     *     if ($getShared) {
     *         return static::getSharedInstance('example');
     *     }
     *
     *     return new \CodeIgniter\Example();
     * }
     */

    public static function getSignedAccessTokenForUser(string $email)
    {
        if (!empty($email)) {
            $issuedAtTime = time();
            $tokenTimeToLive = getenv('JWT_TOKEN_TIME');
            $tokenExpiration = $issuedAtTime + $tokenTimeToLive;

            $jwtBody    =   [
                "iat" => $issuedAtTime,
                "exp" => $tokenExpiration,
                "email" => $email,
            ];

            return JWT::encode($jwtBody, getenv('JWT_SECRET'), getenv('JWT_ALGO'));
        }
        return false;
    }

    public static function getAccessForSignedUser(string $token)
    {
        if (!empty($token)) {
            return JWT::decode($token, new Key(getenv('JWT_SECRET'), getenv('JWT_ALGO')));
        }
        return false;
    }

    public function success(array $json, int $code = ResponseInterface::HTTP_OK, $response)
    {
        $data = [
            'status'            =>  "success",
            'message'           =>  $json['message'],
            'error'             =>  "",
            'data'              =>  $json['data'],
            'response_datetime' =>  Time::now(getenv('TIMEZONE'))->toDateTimeString()
        ];
        return $this->sendResponse($data, $code, $response);
    }

    public function fail(array $json, int $code = ResponseInterface::HTTP_BAD_REQUEST, $response)
    {
        $data = [
            'status'            =>  "failed",
            'errors'            =>  $json['errors'],
            'message'           =>  $json['message'],
            'response_datetime' =>  Time::now(getenv('TIMEZONE'))->toDateTimeString(),
        ];
        return  $this->sendResponse($data, $code, $response);
    }

    protected function sendResponse(array $body, int $code, $response)
    {
        return $response
            ->setStatusCode($code)
            ->setJSON($body);
    }
}
