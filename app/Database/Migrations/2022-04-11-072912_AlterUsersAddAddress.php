<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AlterUsersAddAddress extends Migration
{
    public function up()
    {
        $this->forge->addColumn("users", [
            "address"           => [
                "type"          => "VARCHAR",
                "constraint"    => "120",
                "null"          => true,
                "after"         => "password"
            ],
            "city"              => [
                "type"          => "VARCHAR",
                "constraint"    => "120",
                "null"          => true,
                "after"         => "address"
            ],
            "state"             => [
                "type"          => "VARCHAR",
                "constraint"    => "120",
                "null"          => true,
                "after"         => "city"
            ],
            "country"           => [
                "type"          => "VARCHAR",
                "constraint"    => "120",
                "null"          => true,
                "after"         => "state"
            ]
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('users', ["address", "city", "state", "country"]);
    }
}
